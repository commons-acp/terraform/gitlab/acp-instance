terraform {

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.7"
    }
  }
}

variable "parent_id" {
  description = "parent id of the project"
}
variable "project_name"{
  description = "project name"
}
variable "gitlab_user"{
  description = "gitlab user"
}
variable "gitlab_token"{
  description = "gitlab user token"
}

resource "gitlab_group" "acp-group" {
  name        = var.project_name
  path        = "${var.project_name}-grp"
  parent_id   = var.parent_id
  description = "A project group"
}
// Create a project in the example group
resource "gitlab_project" "infra" {
  name         = "infra"
  description  = "An Infra project"
  namespace_id = gitlab_group.acp-group.id
  initialize_with_readme = false
  shared_runners_enabled = false
  merge_method = "ff"
  only_allow_merge_if_pipeline_succeeds = true
  remove_source_branch_after_merge = true
  default_branch= "master"
}
resource "gitlab_project" "infra_localhost" {
  name         = "infra-localhost"
  description  = "An Infra project for localhost resources"
  namespace_id = gitlab_group.acp-group.id
  initialize_with_readme = false
  shared_runners_enabled = false
  merge_method = "ff"
  only_allow_merge_if_pipeline_succeeds = true
  remove_source_branch_after_merge = true
  default_branch= "master"
}
resource "gitlab_group_variable" "gitlab_user_variable" {
  depends_on= [gitlab_group.acp-group]
  group     = gitlab_group.acp-group.id
  key       = "gitlab_user"
  value     = var.gitlab_user
  protected = false
  masked    = false
}


resource "null_resource" "initialize_repo_infra" {
  provisioner "local-exec" {
    command = "/usr/bin/python3.8 $PATH/initialize.py $TOKEN $PROJECT_ID $PROJECT_USER"

    environment = {
      PATH = path.module
      TOKEN = var.gitlab_token
      PROJECT_ID = gitlab_project.infra.id
      PROJECT_USER = var.gitlab_user
    }
  }
}
resource "null_resource" "initialize_repo_infra_localhost" {
  provisioner "local-exec" {
    command = "/usr/bin/python3.8 $PATH/initialize.py $TOKEN $PROJECT_ID $PROJECT_USER"

    environment = {
      PATH = path.module
      TOKEN = var.gitlab_token
      PROJECT_ID = gitlab_project.infra_localhost.id
      PROJECT_USER = var.gitlab_user
    }
  }
}
